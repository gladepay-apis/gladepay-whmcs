Gladepay WHMCS

# Install 
To install the new module, upload it to the /modules/gateways/ folder of your WHMCS installation.

If the module includes a callback file, that should be uploaded to the /modules/gateways/callback/ folder.

Once uploaded, navigate to Setup > Payment Gateways to activate and configure the new module.