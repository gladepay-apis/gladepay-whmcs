<?php
/**
 * Gladepay Payment Gateway
 *
 */

if (!defined("WHMCS")) {
    die("This file cannot be accessed directly");
}

/**
 * Define module related meta data.
 *
 * Values returned here are used to determine module related capabilities and
 * settings.
 *
 * @see https://developers.whmcs.com/payment-gateways/meta-data-params/
 *
 * @return array
 */
function gladepay_MetaData()
{
    return array(
        'DisplayName' => 'Gladepay Payment Gateway Module',
        'APIVersion' => '1.1', // Use API Version 1.1
        'DisableLocalCredtCardInput' => true,
        'TokenisedStorage' => false,
    );
}

/**
 * Define gladepay gateway configuration options.
 *
 *
 * @return array
 */
function gladepay_config()
{
    return array(
        'FriendlyName' => array(
            'Type' => 'System',
            'Value' => 'Gladepay (Credit/Debit Cards & More)',
        ),
        'gatewayLogs' => array(
            'FriendlyName' => 'Gateway logs',
            'Type' => 'yesno',
            'Description' => 'Tick to enable gateway logs',
            'Default' => '0'
        ),
        'testMode' => array(
            'FriendlyName' => 'Test Mode',
            'Type' => 'yesno',
            'Description' => 'Tick to enable test mode',
            'Default' => '0'
        ),
        'liveMID' => array(
            'FriendlyName' => 'Live Merchant ID',
            'Type' => 'text',
            'Size' => '100',
            'Default' => 'xxxx'
        ),
        'liveMKey' => array(
            'FriendlyName' => 'Live Merchant Key',
            'Type' => 'text',
            'Size' => '100',
            'Default' => 'xxxx'
        ),
        'testMID' => array(
            'FriendlyName' => 'Test Merchant ID',
            'Type' => 'text',
            'Size' => '100',
            'Default' => 'xxxx'
        ),
        'testMKey' => array(
            'FriendlyName' => 'Test Merchant Key',
            'Type' => 'text',
            'Size' => '100',
            'Default' => 'xxxx'
        )
    );
}

/**
 * Payment link.
 *
 * Required by third party payment gateway modules only.
 *
 * Defines the HTML output displayed on an invoice. Typically consists of an
 * HTML form that will take the user to the payment gateway endpoint.
 *
 * @param array $params Payment Gateway Module Parameters
 *
 * @see https://developers.whmcs.com/payment-gateways/third-party-gateway/
 *
 * @return string
 */
function gladepay_link($params)
{
    // Gateway Configuration Parameter
    $testMode = $params['testMode'];

    if($testMode == 'on'){
        $mid = $params['testMID'];
        $mkey = $params['testMKey'];
        $checkoutEndpoint = "https://demo.api.gladepay.com";
    }else{
        $mid = $params['liveMID'];
        $mkey = $params['liveMKey'];
        $checkoutEndpoint = "https://api.gladepay.com";
    }

    // Invoice Parameters
    $invoiceId = $params['invoiceid'];
    $description = $params["description"];
    $amount = $params['amount'];
    $currency = $params['currency'];

    //validate the currency
    if (!in_array(strtoupper($currency), [ 'NGN', 'USD'])) {
        return ("<b style='color:red;padding:2px;display: block;margin:2px;border-radius:2px;font-size:13px;'>Sorry only NGN and USD payments are accepted by this version. <i>$currency</i> not yet supported.</b>");
    }

    // Client Parameters
    $firstname = $params['clientdetails']['firstname'];
    $lastname = $params['clientdetails']['lastname'];
    $email = $params['clientdetails']['email'];
    $address1 = $params['clientdetails']['address1'];
    $address2 = $params['clientdetails']['address2'];
    $city = $params['clientdetails']['city'];
    $state = $params['clientdetails']['state'];
    $postcode = $params['clientdetails']['postcode'];
    $country = $params['clientdetails']['country'];
    $phone = $params['clientdetails']['phonenumber'];

    // System Parameters
    $companyName = $params['companyname'];
    $systemUrl = $params['systemurl'];
    $returnUrl = $params['returnurl'];
    $langPayNow = $params['langpaynow'];
    $moduleDisplayName = $params['name'];
    $moduleName = $params['paymentmethod'];
    $whmcsVersion = $params['whmcsVersion'];

    $postfields = array();
    $postfields['invoice_id'] = $invoiceId;
    $postfields['description'] = $description;
    $postfields['amount'] = $amount;
    $postfields['currency'] = $currency;
    $postfields['first_name'] = $firstname;
    $postfields['last_name'] = $lastname;
    $postfields['email'] = $email;
    $postfields['address1'] = $address1;
    $postfields['address2'] = $address2;
    $postfields['city'] = $city;
    $postfields['state'] = $state;
    $postfields['postcode'] = $postcode;
    $postfields['country'] = $country;
    $postfields['phone'] = $phone;
    $postfields['callback_url'] = $systemUrl . '/modules/gateways/callback/gladepay.php';
    $postfields['return_url'] = $returnUrl;

    $meta = json_encode($postfields);

    $orderRef = $invoiceId . '_' .time();

    $isSSL = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443);
    
    $callbackUrl = 'http' . ($isSSL ? 's' : '') . '://' . $_SERVER['HTTP_HOST'] .
        substr($_SERVER['REQUEST_URI'], 0, strrpos($_SERVER['REQUEST_URI'], '/')) .
        '/modules/gateways/callback/gladepay.php?' . 
        http_build_query(
            array(
                'invoiceid' => $invoiceId,
                'orderRef' => $orderRef
            )
        );

    $code = '
        <style>
            .pay{
                min-width: 200px;
                height: 40px;
                margin: 0px auto 10px;
                padding: 8px 16px;
                background: linear-gradient(135deg, #337ab7, #353D7B);
                border: none;
                font-family: \'Montserrat\', sans-serif;
                font-weight: 500;
                font-size: 14px;
                color: #fff;
                text-transform: uppercase;
                position: relative;
                -webkit-border-radius: 4px;
                -moz-border-radius: 4px;
                border-radius: 4px;
                -webkit-transition: all 0.3s;
                -moz-transition: all 0.3s;
                -ms-transition: all 0.3s;
                -o-transition: all 0.3s;
                transition: all 0.3s;
            }
        </style>
        <script type="text/javascript" src="' . $checkoutEndpoint . '/checkout.js"></script>
        <script>
            (typeof $ === \'undefined\') && document.write("<scr" + "ipt type=\"text\/javascript\" '.
            'src=\"https:\/\/code.jquery.com\/jquery-1.12.3.min.js\"><\/scr" + "ipt>");
        </script>
        <script>
            $(function() {
                var paymentMethod = $(\'select[name="gateway"]\').val();
                if (paymentMethod === \'gladepay\') {
                    
                    var toAppend = \'<button class="pay" type="button"'. 
                   ' onclick="pay()"'.
                   ' style="">'.
                    addslashes($params['langpaynow']).'</button>'.
                    '\';

                    $(\'.payment-btn-container\').append(toAppend);
                    
                }
            });

            function pay(){
                initPayment({
                    MID:"'.$mid.'",
                    email: "'.$email.'",
                    firstname:"'.$firstname.'",
                    lastname:"'.$lastname.'",
                    description: "'.$description.'",
                    title: "",
                    amount: '.$amount.',
                    country: "NG",
                    currency: "'.$currency.'",
                    meta: \''.$meta.'\',
                    onclose: function() {
                
                    },
                    callback: function(response) {
                        response = JSON.stringify(response);
                        window.location.href = \''.$callbackUrl.'&response=\'+response;
                    }
                });
            }
        </script>
    ';

    return $code;
}
