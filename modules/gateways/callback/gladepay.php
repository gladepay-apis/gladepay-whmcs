<?php
/**
 * WHMCS Sample Payment Callback File
 *
 * This sample file demonstrates how a payment gateway callback should be
 * handled within WHMCS.
 *
 * It demonstrates verifying that the payment gateway module is active,
 * validating an Invoice ID, checking for the existence of a Transaction ID,
 * Logging the Transaction for debugging and Adding Payment to an Invoice.
 *
 * For more information, please refer to the online documentation.
 *
 * @see https://developers.whmcs.com/payment-gateways/callbacks/
 *
 * @copyright Copyright (c) WHMCS Limited 2017
 * @license http://www.whmcs.com/license/ WHMCS Eula
 */

use WHMCS\Database\Capsule;

// Require libraries needed for gateway module functions.
require_once __DIR__ . '/../../../init.php';
require_once __DIR__ . '/../../../includes/gatewayfunctions.php';
require_once __DIR__ . '/../../../includes/invoicefunctions.php';

// Detect module name from filename.
$gatewayModuleName = basename(__FILE__, '.php');

// Fetch gateway configuration parameters.
$gatewayParams = getGatewayVariables($gatewayModuleName);

// Die if module is not active.
if (!$gatewayParams['type']) {
    die("Module Not Activated");
}

//config
if($gatewayParams['testMode'] == 'on'){
    $mid = $gatewayParams['testMID'];
    $mkey = $gatewayParams['testMKey'];
    $apiEndpoint = "https://demo.api.gladepay.com";
}else{
    $mid = $gatewayParams['liveMID'];
    $mkey = $gatewayParams['liveMKey'];
    $apiEndpoint = "https://api.gladepay.com";
}

$success = false;
$paymentAmount = 0;
$paymentFee = 0;

$isSSL = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443);
$invoice_url = 'http' . ($isSSL ? 's' : '') . '://' . $_SERVER['HTTP_HOST'] . substr(str_replace('/admin/', '/', $_SERVER['REQUEST_URI']), 0, strrpos($_SERVER['REQUEST_URI'], '/'));


if(isset($_GET['invoiceid']) && isset($_GET['orderRef']) && isset($_GET['response'])){
    //get the status of the transaction from the server.
    $invoiceId = $_GET['invoiceid'];

    //clean response data before getting the transaction reference
    $response = json_decode(stripslashes(html_entity_decode($_GET['response'])));
    $transactionId = $response->txnRef;
    
    $query = query($transactionId);
    $result = json_decode($query, true);

    if($result['status'] == 200){
        if(strtolower($result['txnStatus']) == 'successful'){
            $success = true;
            $paymentAmount = $result['chargedAmount'];
        }
    }

    $transactionStatus = $success ? 'Success' : 'Failure';
}else{
    die('Invalid transaction information');
}

/**
 * Validate Callback Invoice ID.
 *
 * Checks invoice ID is a valid invoice number. Note it will count an
 * invoice in any status as valid.
 *
 * Performs a die upon encountering an invalid Invoice ID.
 *
 * Returns a normalised invoice ID.
 *
 * @param int $invoiceId Invoice ID
 * @param string $gatewayName Gateway Name
 */
$invoiceId = checkCbInvoiceID($invoiceId, $gatewayParams['name']);

/**
 * Check Callback Transaction ID.
 *
 * Performs a check for any existing transactions with the same given
 * transaction number.
 *
 * Performs a die upon encountering a duplicate.
 *
 * @param string $transactionId Unique Transaction ID
 */
checkCbTransID($transactionId);

/**
 * Log Transaction.
 *
 * Add an entry to the Gateway Log for debugging purposes.
 *
 * The debug data can be a string or an array. In the case of an
 * array it will be
 *
 * @param string $gatewayName        Display label
 * @param string|array $debugData    Data to log
 * @param string $transactionStatus  Status
 */

if ($gatewayParams['gatewayLogs'] == 'on') {
    logTransaction($gatewayParams['name'], $query, $transactionStatus);
}

if ($success) {

    //the amount has to match the required amount to get paid
    $getAmount = Capsule::table('tblinvoices')->where('id', '=', $invoiceId)->first();
    $amount = $getAmount->total;

    if ($gatewayParams['convertto']) {
        $result = select_query("tblclients", "tblinvoices.invoicenum,tblclients.currency,tblcurrencies.code", array("tblinvoices.id" => $invoiceId), "", "", "", "tblinvoices ON tblinvoices.userid=tblclients.id INNER JOIN tblcurrencies ON tblcurrencies.id=tblclients.currency");
        $data = mysql_fetch_array($result);
        $currency_id = $data['currency'];

        $convert_to_amount = convertCurrency($paymentAmount, $gatewayParams['convertto'], $currency_id);
        $paymentAmount = format_as_currency($convert_to_amount);
    }

    if($paymentAmount >= $amount){
        /**
         * Add Invoice Payment.
         *
         * Applies a payment transaction entry to the given invoice ID.
         *
         * @param int $invoiceId         Invoice ID
         * @param string $transactionId  Transaction ID
         * @param float $paymentAmount   Amount paid (defaults to full balance)
         * @param float $paymentFee      Payment fee (optional)
         * @param string $gatewayModule  Gateway module name
         */
        addInvoicePayment(
            $invoiceId,
            $transactionId,
            $paymentAmount,
            $paymentFee,
            $gatewayModuleName
        );

        sendBackToInvoice();
    }else{
        //the user has made an incomplete transaction
        die('You have made an Incomplete payment.');
    }

}else{
    //failed
    // echo "failed";
    sendBackToInvoice();
}

function query($txnId){
    $curl = curl_init();

    $data = [
        'action' => 'verify',
        'txnRef' => $txnId
    ];

    //collect the configuration to query
    $url = $GLOBALS['apiEndpoint'].'/payment';
    $mkey = $GLOBALS['mkey'];
    $mid = $GLOBALS['mid'];

    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_TIMEOUT => 300,
        CURLOPT_ENCODING => "",
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "PUT",
        CURLOPT_POSTFIELDS => json_encode($data),
        CURLOPT_HTTPHEADER => array(
          "content-type: application/json",
          "key: {$mkey}",
          "mid: {$mid}",
        ),
      ));
      
      $response = curl_exec($curl);
      $err = curl_error($curl);
      
      curl_close($curl);
      
      if ($err) {
        return "Error #:" . $err;
      } else {
        return  $response;
      }
}

function sendBackToInvoice(){
    $invoice_url = $GLOBALS['invoice_url'];
    $invoiceId = $GLOBALS['invoiceId'];

    $url = $invoice_url.'/../../../viewinvoice.php?id='.rawurlencode($invoiceId);
    
    header('Location: '.$url);
}
